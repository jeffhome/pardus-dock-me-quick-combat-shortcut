// ==UserScript==
// @name        Dock-Me-Quick From Combat
// @namespace   http://userscripts.xcom-alliance.info/
// @description Dock right out of combat when fighting on your building or when in the Nav screen using a shortcut key [ * ]
// @version     1.1
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/ship2ship_combat.php*
// @include     http*://*.pardus.at/main.php*
// @updateURL 	http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/pardus_dock_me_quick_combat_shortcut.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/pardus_dock_me_quick_combat_shortcut.user.js
// @icon 		http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/icon.png
// ==/UserScript==
