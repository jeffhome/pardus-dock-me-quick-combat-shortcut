// ==UserScript==
// @name        Dock-Me-Quick From Combat
// @namespace   http://userscripts.xcom-alliance.info/
// @description Dock right out of combat when fighting on your building or when in the Nav screen using a shortcut key [ * ]
// @version     1.1
// @author      Miche (Orion) / Sparkle (Artemis)
// @include     http*://*.pardus.at/ship2ship_combat.php*
// @include     http*://*.pardus.at/main.php*
// @updateURL 	http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/pardus_dock_me_quick_combat_shortcut.meta.js
// @downloadURL http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/pardus_dock_me_quick_combat_shortcut.user.js
// @icon 		http://userscripts.xcom-alliance.info/dock_me_quick_combat_shortcut/icon.png
// ==/UserScript==

/*****************************************************************************************
	Version Information

	20/03/2013 (Version 1.0)
	  - Released this simple keyboard shortcut

	When in ship to ship combat on your own building, press "*" once to dock your ship. 
	Change the key for this shortcut by changing the KEY_PRESS_FOR_DOCKING variable below.

	13/07/2013 (Version 1.1)
	 - Changed to also work when not in combat on your own MO

*****************************************************************************************/

var KEY_PRESS_FOR_DOCKING = '*';

/*****************************************************************************************
	There is no need to modify anything further in this file
*****************************************************************************************/

function handleKeyPressToDock(e) {
	if (window.name == '' || e.ctrlKey || e.metaKey || e.altKey || e.target.nodeName == 'INPUT' || e.target.nodeName == 'TEXTAREA') return;

	if (String.fromCharCode(e.which).toLowerCase() == KEY_PRESS_FOR_DOCKING.toLowerCase()) {
		top.window.location = 'game.php?logout=1';
	}
};

window.addEventListener("keypress", handleKeyPressToDock, true);